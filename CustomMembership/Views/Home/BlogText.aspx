﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	BlogText
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%=  ViewData["BlogTitle"]%> </h2>


    <asp:Panel ID="Panel1" runat="server" >
    <%=  ViewData["BlogText"]%>
    </asp:Panel>
    <h5><%=  ViewData["User"]%> </h5>
</asp:Content>
