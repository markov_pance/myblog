﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Private
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

   
    Welcome <b><%: Page.User.Identity.Name %></b>!
    This is a page only with your blogs!
    <%= Html.ActionLink("new blog", "EditBlog",new { @id = 0 })%>

    <ol>
    <% foreach (CustomMembership.Models.BlogClass Blog
           in (List<CustomMembership.Models.BlogClass>)ViewData["BlogTable"])
    { %>
        <li><%= Blog.UserName%>, <%= Html.ActionLink(Blog.Title, "EditBlog", new { @id = Blog.BlogID })%></li>
    <% } %>
    </ol>

</asp:Content>
