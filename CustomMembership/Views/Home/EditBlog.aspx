﻿<%@ Page Title="" Debug="true" AutoEventWireup="true" ValidateRequest="false" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

  
    


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edit blog
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm()) { %>
<link rel="Stylesheet" type="text/css" href="../../Content/jHtmlArea.css" />
<script type="text/javascript" src="../../Scripts/jquery-1.4.1.min.js"></script>
<script type="text/javascript" src="../../Scripts/jHtmlArea-0.6.0.min.js"></script>
    <style type="text/css">
        /* body { background: #ccc;} */
        div.jHtmlArea .ToolBar ul li a.custom_disk_button 
        {
            background: url(images/disk.png) no-repeat;
            background-position: 0 0;
        }
        
        div.jHtmlArea { border: solid 1px #ccc; }
    </style>

    <script type="text/javascript">

        function myfunction(form) {
            document.getElementById('txtBody').value = $('#txtDefaultHtmlArea').htmlarea('toHtmlString');
        }



        $(function () {
            //$("textarea").htmlarea(); // Initialize all TextArea's as jHtmlArea's with default values

            $("#txtDefaultHtmlArea").htmlarea(); // Initialize jHtmlArea's with all default values

            $("#txtCustomHtmlArea").htmlarea({
                // Override/Specify the Toolbar buttons to show
                toolbar: [
                    ["bold", "italic", "underline", "|", "forecolor"],
                    ["h1", "h2", "h3", "h4", "h5", "h6"],
                    ["link", "unlink", "|", "image"],
                    [{
                        // This is how to add a completely custom Toolbar Button
                        css: "custom_disk_button",
                        text: "Save",
                        action: function (btn) {
                            // 'this' = jHtmlArea object
                            // 'btn' = jQuery object that represents the <A> "anchor" tag for the Toolbar Button
                            alert('SAVE!\n\n' + this.toHtmlString());
                        }
                    }]
                ],

                // Override any of the toolbarText values - these are the Alt Text / Tooltips shown
                // when the user hovers the mouse over the Toolbar Buttons
                // Here are a couple translated to German, thanks to Google Translate.
                toolbarText: $.extend({}, jHtmlArea.defaultOptions.toolbarText, {
                    "bold": "fett",
                    "italic": "kursiv",
                    "underline": "unterstreichen"
                }),

                // Specify a specific CSS file to use for the Editor
                css: "../../Content//jHtmlArea.Editor.css",

                // Do something once the editor has finished loading
                loaded: function () {
                }
            });
        });
    </script>


<div class="editor-label">
  Title
 </div>
<input type="text" name="title" style="width:420px" value= "<%: ViewData["BlogTitle"] %>" />
<div class="editor-label">
  Body
 </div>
<input type="hidden" name="Body" id="txtBody"    />
<input type="hidden" name="BlogID"  value ="<%: ViewData["BlogID"] %>"    />
<input type="hidden" name="UserName"  value="<%: Page.User.Identity.Name %>"/>
<textarea id="txtDefaultHtmlArea"   onclick="myfunction(this.form)"  cols="50" rows="15"><%:ViewData["BlogText"]%></textarea>
<input type="submit" value="Submit" onclick="myfunction(this.form)" name="btnSubmit"  />
<input type="submit" value="Delete" onclick="myfunction(this.form)" name="btnSubmit"  />
 <% } %>
</asp:Content>
