﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="MvcContrib.UI.Grid" %>
<%@ Import Namespace="MvcContrib.UI.Grid.ActionSyntax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Home Page
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%= Html.ActionLink("Edit your blog text", "Protected", new { @id = Page.User.Identity.Name })%>
<ol>
<% foreach (CustomMembership.Models.BlogClass Blog
       in (List<CustomMembership.Models.BlogClass>)ViewData["BlogTable"])
{ %>
    <li><%= Blog.UserName%>, <%= Html.ActionLink(Blog.Title, "BlogText", new { @id = Blog.BlogID })%></li>
<% } %>
</ol>


</asp:Content>

