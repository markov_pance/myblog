﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomMembership.Models;
using System.Data;

namespace CustomMembership.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
           
            BlogClass tmpblog = new BlogClass();
            DataTable dt = new DataTable(); 
             ViewData["BlogTable"]  = tmpblog.FillALLBlog();
           
            
           
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        [Authorize]
        public ActionResult Protected(string ID)
        {
            BlogClass tmpblog = new BlogClass();
            tmpblog.UserName = ID;
            ViewData["BlogTable"] = tmpblog.FillUserBlog();
            return View();
        }
        

        [Authorize]
        public ActionResult BlogText(string ID)
        {
            BlogClass tmpBlog = new BlogClass();
            tmpBlog.BlogID = Convert.ToInt64(ID);
            tmpBlog.FillThisBlog();
            ViewData["BlogText"] = tmpBlog.Body;
            ViewData["BlogTitle"] = tmpBlog.Title;
            ViewData["User"] = tmpBlog.UserName;
           // ViewData["BlogID"] = ID;
            return View();
        }

        [Authorize]
        [ValidateInput(false)]
        public ActionResult EditBlog(string ID)
        {
            if (ID != "0")
            {
                BlogClass tmpBlog = new BlogClass();
                tmpBlog.BlogID = Convert.ToInt64(ID);
                tmpBlog.FillThisBlog();
                ViewData["BlogText"] = tmpBlog.Body;
                ViewData["BlogTitle"] = tmpBlog.Title;
                ViewData["User"] = tmpBlog.UserName;
                ViewData["BlogID"] = tmpBlog.BlogID;
            }
            else
            {
                ViewData["BlogText"] = "";
                ViewData["BlogTitle"] = "";
                ViewData["BlogID"] = 0;
            }
            return View();
        }


       
        [HttpPost]
        [ValidateInput(false)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditBlog(BlogClass model, string btnSubmit)
        {
            
            string tmp = btnSubmit;
            switch (btnSubmit)
            {
                case "Submit":
                   model.InsertBlog(); /*Insert or update blog */
                    break;
                case "Delete":
                    model.DeleteBlog(); /* Delete blog */
                    break;
            }

            

            return RedirectToAction("Protected", "Home");


        }
    }
}
