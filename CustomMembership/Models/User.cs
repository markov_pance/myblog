﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq;
using System.Configuration;

namespace CustomMembership.Models
{
    public class User
    {
        private Table<UserObj> usersTable;
        private DataContext context;

        public User()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["AppDb"].ConnectionString;
            context = new DataContext(connectionString);
            usersTable = context.GetTable<UserObj>();
        }

        public UserObj GetUserObjByUserName(string userName, string passWord)
        {
            UserObj user = usersTable.SingleOrDefault(u => u.UserName == userName && u.Password == passWord);
            return user;
        }

        public UserObj GetUserObjByUserName(string userName)
        {
            UserObj user = usersTable.SingleOrDefault(u => u.UserName == userName);
            return user;
        }

        public IEnumerable<UserObj> GetAllUsers()
        {
            return usersTable.AsEnumerable();
        }

        public int RegisterUser(UserObj userObj)
        {
            UserObj user = new UserObj();
            user.UserName = userObj.UserName;
            user.Password = userObj.Password;
            user.Email = userObj.Email;

            usersTable.InsertOnSubmit(user);
            context.SubmitChanges();

            return user.UserID;
        }
    }
}
