﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq.Mapping;

namespace CustomMembership.Models
{
    [Table(Name="Users")]
    public class UserObj
    {
        [Column(IsPrimaryKey=true, IsDbGenerated = true, AutoSync=AutoSync.OnInsert)]
        public int UserID { get; set; }
        [Column] public string UserName { get; set; }
        [Column] public string Password { get; set; }
        [Column] public string Email { get; set; }
    }
}
