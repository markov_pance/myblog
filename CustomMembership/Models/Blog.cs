﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;

namespace CustomMembership.Models
{

    public class BlogClass
    {
       #region Declaration

        public int UserID { get; set; }
        public Int64 BlogID { get; set; }
        public string UserName { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }


       #endregion

       #region Functions

        public bool InsertBlog()
        {
            string ConnStr = ConfigurationManager.ConnectionStrings["AppDb"].ConnectionString;
            bool Result = true; 
            try
            {
                Communication.Connection cnn = new Communication.Connection(ConnStr);
                cnn.Clear();
                cnn.CmdType = CommandType.StoredProcedure;
                cnn.Add("@BlogId", SqlDbType.BigInt, BlogID);
                cnn.Add("@UserName", SqlDbType.NVarChar, UserName);
                cnn.Add("@Title", SqlDbType.NVarChar, Title);
                cnn.Add("@Body", SqlDbType.NVarChar, (Body is DBNull) ? "": Body);
                cnn.Command = "InsertNewBlog";
                cnn.Execute();
            }
            catch (Exception e)
            {
                //zapisi vo nekoj log   e.Message;
                Result = false;
            }

            return Result;
        }
        public bool DeleteBlog()
        {
            string ConnStr = ConfigurationManager.ConnectionStrings["AppDb"].ConnectionString;
            bool Result = true;
            try
            {
                Communication.Connection cnn = new Communication.Connection(ConnStr);
                cnn.Clear();
                cnn.CmdType = CommandType.StoredProcedure;
                cnn.Add("@BlogID", SqlDbType.BigInt, BlogID);
                cnn.Command = "DeleteBlog";
                cnn.Execute();
            }
            catch (Exception e)
            {
                //zapisi vo nekoj log   e.Message;
                Result = false;
            }

            return Result;
        }
        public List<BlogClass> FillALLBlog()
        {
            string ConnStr = ConfigurationManager.ConnectionStrings["AppDb"].ConnectionString;
            DataSet ds = new DataSet("Blog");
            List<BlogClass> BlogObj = new List<BlogClass>();
            BlogClass tmpBlog ;
            try
            {
                Communication.Connection cnn = new Communication.Connection(ConnStr);
                cnn.Clear();
                cnn.CmdType = CommandType.StoredProcedure;
                cnn.Add("@BlogID", SqlDbType.BigInt, 0);
                cnn.Add("@UserName", SqlDbType.NVarChar, "");
                cnn.Command = "FillBlog";
                cnn.Execute(ref ds,"Blog");
                DataTable table = ds.Tables["Blog"];
                foreach (DataRow row in table.Rows)
                {
                    tmpBlog = new BlogClass();
                    tmpBlog.BlogID = Convert.ToInt64(row.ItemArray[0].ToString());
                    tmpBlog.UserID = Convert.ToInt32(row.ItemArray[1].ToString());
                    tmpBlog.UserName = row.ItemArray[2].ToString();
                    tmpBlog.Title = row.ItemArray[3].ToString();
                    tmpBlog.Body = row.ItemArray[4].ToString();
                    BlogObj.Add(tmpBlog);
                }
                
            }
            catch (Exception e)
            {
                //zapisi vo nekoj log   e.Message;

            }

            return BlogObj;// ds.Tables["Blog"];

        }
        public void FillThisBlog()
        {
            string ConnStr = ConfigurationManager.ConnectionStrings["AppDb"].ConnectionString;
            BlogClass tmpBlog = new BlogClass();
            DataSet ds = new DataSet("Blog");
            try
            {
                Communication.Connection cnn = new Communication.Connection(ConnStr);
                cnn.Clear();
                cnn.CmdType = CommandType.StoredProcedure;
                cnn.Add("@BlogID", SqlDbType.BigInt, BlogID);
                cnn.Add("@UserName", SqlDbType.NVarChar, "");
                cnn.Command = "FillBlog";
                cnn.Execute(ref ds, "Blog");

                //tmpBlog.BlogID = Convert.ToInt64(ds.Tables["Blog"].Rows[0].ItemArray[0].ToString());
               UserID = Convert.ToInt32(ds.Tables["Blog"].Rows[0].ItemArray[1].ToString());
               UserName = ds.Tables["Blog"].Rows[0].ItemArray[2].ToString();
               Title = ds.Tables["Blog"].Rows[0].ItemArray[3].ToString();
               Body = ds.Tables["Blog"].Rows[0].ItemArray[4].ToString();

            }
            catch (Exception e)
            {
                //zapisi vo nekoj log   e.Message;

            }


        }
        public List<BlogClass> FillUserBlog()
        {
            string ConnStr = ConfigurationManager.ConnectionStrings["AppDb"].ConnectionString;
            DataSet ds = new DataSet("Blog");
            List<BlogClass> BlogObj = new List<BlogClass>();
            BlogClass tmpBlog;
            try
            {
                Communication.Connection cnn = new Communication.Connection(ConnStr);
                cnn.Clear();
                cnn.CmdType = CommandType.StoredProcedure;
                cnn.Add("@BlogID", SqlDbType.BigInt, 0);
                cnn.Add("@UserName", SqlDbType.NVarChar, UserName);
                cnn.Command = "FillBlog";
                cnn.Execute(ref ds, "Blog");
                DataTable table = ds.Tables["Blog"];
                foreach (DataRow row in table.Rows)
                {
                    tmpBlog = new BlogClass();
                    tmpBlog.BlogID = Convert.ToInt64(row.ItemArray[0].ToString());
                    tmpBlog.UserID = Convert.ToInt32(row.ItemArray[1].ToString());
                    tmpBlog.UserName = row.ItemArray[2].ToString();
                    tmpBlog.Title = row.ItemArray[3].ToString();
                    tmpBlog.Body = row.ItemArray[4].ToString();
                    BlogObj.Add(tmpBlog);
                }

            }
            catch (Exception e)
            {
                //zapisi vo nekoj log   e.Message;

            }

            return BlogObj;// ds.Tables["Blog"];

        }

       #endregion
    }
}
