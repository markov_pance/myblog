﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Communication
{
    public class Connection
    {
        #region Declaration
        private SqlConnection _cnn;
        private SqlCommand _cmd;
        private SqlDataAdapter _da;
        private int _Back;
        private bool _IsThreadOn = false;
        #endregion

        #region Properties
        public System.Data.CommandType CmdType
        {
            get { return _cmd.CommandType; }
            set { _cmd.CommandType = value; }
        }
        public string Command
        {
            get { return _cmd.CommandText; }
            set { _cmd.CommandText = value; }
        }
        public bool IsThreadOn
        {
            get { return _IsThreadOn; }
            set { _IsThreadOn = value; }
        }
        #endregion

        #region Constructor
        public Connection(string ConnStr)
        {
            _cnn = new SqlConnection(ConnStr);
            _cmd = new SqlCommand("", _cnn);
            _cmd.CommandType = CommandType.StoredProcedure;
        }
        ~Connection()
        {
            Cleaning();
        }
        #endregion

        #region Functions
        public void Dispose()
        {
            Cleaning();
        }
        public int Execute()
        {
            if (_cnn.State == ConnectionState.Closed
                    || _cnn.State == ConnectionState.Broken)
                _cnn.Open();
            _Back = _cmd.ExecuteNonQuery();
            return _Back;
        }
        public int Execute(ref DataSet inDS)
        {
            if (_cnn.State == ConnectionState.Closed
                   || _cnn.State == ConnectionState.Broken)
                _cnn.Open();
            _da = new System.Data.SqlClient.SqlDataAdapter(_cmd);
            _Back = _da.Fill(inDS);
            return _Back;
        }
        public int Execute(ref DataSet inDS, string inTableName)
        {
            if (_cnn.State == ConnectionState.Closed
                   || _cnn.State == ConnectionState.Broken)
                _cnn.Open();
            _da = new System.Data.SqlClient.SqlDataAdapter(_cmd);
            _Back = _da.Fill(inDS, inTableName);
            return _Back;
        }
        public void Cleaning()
        {
            if (_cnn != null)
            {
                //if(_cnn.State != ConnectionState.Closed) 
                //_cnn.Close();
                //_cnn.Dispose();
            }
        }
        #endregion

        #region Parameters
        public void Clear()
        {
            _cmd.Parameters.Clear();
        }
        public void Add(string ParamName, SqlDbType ParamType, object ParamValue)
        {
            _cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter(ParamName, ParamType)).Value = ParamValue;
        }
        public void Add(string ParamName, SqlDbType ParamType, int ParamLen, object ParamValue)
        {
            _cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter(ParamName, ParamType, ParamLen)).Value = ParamValue;
        }
        #endregion
    }
}
